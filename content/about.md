+++
title = "Sobre"

[extra]
no_header = true
+++

Meu nome é Kaleb, 22 anos ,bacharel em Ciência e Tecnologia e Engenharia de Informação. Adoro "Teoria da Informação" ,"Processamento de sinais digitais" (principalmente para imagem e vídeo). Sou fã de programação funcional, "DevOps" e "PLT". Tolkien, Gaiman, Phillip Dick, William Gibson, Robert Jordan e Lima Barreto, estão entre meus autores favoritos. Aqui vou buscar falar um pouco de ciência e tecnologia de uma forma genérica e bem acessível.
Obs: Estou migrando do Medium e usando o [Zola][zola], um gerador de sites feito em Rust



[zola]: https://getzola.org
[original]: https://github.com/victoriadotdev/hugo-theme-sam
[hugo]: https://gohugo.io
[repository]: https://github.com/janbaudisch/zola-sam
